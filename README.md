# Beneski Building Lab Environmental Monitoring

## Description
We have installed data loggers in two analytical labs in the Beneski Building at Amherst College. These monitors collect the time, relative humidity (%), and temperature (*C) every one to five minutes. Data is then analyzed to make sure that the labs are within the site requirement specs for the individual instruments. This is especially critical over long runs, where dramatic drift in the lab environmental conditions has a direct impact on the quality of collected data.

New updates:
- The data import script is now written as a function. It allows for the data collection interval to be dictated as a function input, increasing the versatility of the data import.
- New plot types to quickly understand how often the lab environment is exceeding manufacturer constraints.

## Dependencies
### R Libraries
 - tidyverse
 - dplyr
- ggplot2
- ggthemes
- scales
- lubridate
- plotly
- dotenv

## Contributing and Support
See something that could work cleaner or have a new feature idea, please feel free to contribute to the project!

If you have a suggestion that would make this better, please fork the repo and create a pull request. 

Here are the steps!
1) Fork the Project
2) Create your Branch 
3) Commit your Changes
4) Push to the Branch
5) Open a Pull Request

If you experience issues with the code, support can be sought by emailing hbrooks@amherst.edu.

## Authors and acknowledgment
Written by Hanna L Brooks. Last update: 2024.

Please use the citation file format (CFF) file when citing this work.

## License
Code is licensed with a MIT License. See license section for more information.
